package com.mycompany.app;

import com.github.tomaslanger.chalk.Chalk;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	System.out.println("This message is " + Chalk.on("IMPORTANT").red().underline());
    }
}
